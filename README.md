### Workzone REST API OpenAPI spec

The supplied yaml file can be used to generate Typescript interfaces or a complete REST client in your preferred language.

### Questions and support

See https://www.izymes.com
