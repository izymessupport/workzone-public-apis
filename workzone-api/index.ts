/**
 * Workzone - REST API
 * 0.0.1
 * DO NOT MODIFY - This file has been generated using oazapfts.
 * See https://www.npmjs.com/package/oazapfts
 */
import * as Oazapfts from "oazapfts/lib/runtime";
import * as QS from "oazapfts/lib/runtime/query";
export const defaults: Oazapfts.RequestOpts = {
    baseUrl: "http://localhost:7990/bitbucket/rest/workzoneresource/latest",
};
const oazapfts = Oazapfts.runtime(defaults);
export const servers = {
    developmentUrl: "http://localhost:7990/bitbucket/rest/workzoneresource/latest"
};
export type SignApprove = {
    signapproveUser?: string;
    signapprovePwd?: string;
    pullReqId?: number;
    repoSlug?: string;
    projectKey?: string;
};
export type RestApplicationUser = {
    links?: {
        empty?: boolean;
    };
    emailAddress?: string;
    slug?: string;
    name?: string;
    id?: number;
    "type"?: "NORMAL" | "SERVICE";
    displayName?: string;
    active?: boolean;
    avatarUrl?: string;
    empty?: boolean;
};
export type RestPullRequestSignature = {
    eventType?: "APPROVED" | "SIGNED" | "UNAPPROVED";
    projectKey?: string;
    repoSlug?: string;
    pullRequestId?: number;
    user?: {
        links?: {
            empty?: boolean;
        };
        emailAddress?: string;
        slug?: string;
        name?: string;
        id?: number;
        "type"?: "NORMAL" | "SERVICE";
        displayName?: string;
        active?: boolean;
        avatarUrl?: string;
        empty?: boolean;
    };
    dateTime?: string;
    comment?: string;
};
export type GlobalConfig = {
    enableReviewerNotifications?: boolean;
    enableMergeConditionVeto?: boolean;
    enableNeedsWorkVeto?: boolean;
    enableImmediateMerge?: boolean;
    disablePullRequestSignatures?: boolean;
    reviewersGroupMembersLimit?: number;
    groupsPerUserLimit?: number;
    maxPullRequestAgeDays?: number;
    auditLogPriority?: string;
    coverageLevel?: string;
    enableRescopeConflictNotifications?: boolean;
};
export type RestFilePathReviewers = {
    filePathPattern?: string;
    includeExcludeEnabled?: boolean;
    includeFilePathPattern?: string[];
    excludeFilePathPattern?: string[];
    users?: {
        [key: string]: object;
    }[];
    approvalCount?: number;
    groups?: string[];
    groupQuota?: number;
    mandatoryUsers?: {
        [key: string]: object;
    }[];
    mandatoryApprovalCount?: number;
    mandatoryGroups?: string[];
    mandatoryGroupQuota?: number;
};
export type RestBranchReviewers = {
    projectKey?: string;
    repoSlug?: string;
    refName?: string;
    refPattern?: string;
    srcRefName?: string;
    srcRefPattern?: string;
    users?: {
        [key: string]: object;
    }[];
    groups?: string[];
    mandatoryUsers?: {
        [key: string]: object;
    }[];
    mandatoryGroups?: string[];
    topSuggestedReviewers?: number;
    daysInPast?: number;
    filePathReviewers?: RestFilePathReviewers[];
};
export type RestBranchSignapprovers = {
    projectKey?: string;
    repoSlug?: string;
    refName?: string;
    refPattern?: string;
    srcRefName?: string;
    srcRefPattern?: string;
    users?: {
        [key: string]: object;
    }[];
    groups?: string[];
    addAsReviewers?: boolean;
};
export type RestBranchAutoMergers = {
    projectKey?: string;
    repoSlug?: string;
    refName?: string;
    refPattern?: string;
    srcRefName?: string;
    srcRefPattern?: string;
    automergeUsers?: {
        [key: string]: object;
    }[];
    approvalQuotaEnabled?: boolean;
    approvalQuota?: string;
    approvalCount?: number;
    mandatoryApprovalCount?: number;
    deleteSourceBranch?: boolean;
    watchBuildResult?: boolean;
    watchTaskCompletion?: boolean;
    requiredBuildsCount?: number;
    requiredSignaturesCount?: number;
    groupQuota?: number;
    mergeCondition?: string;
    mergeStrategyId?: string;
    ignoreContributingReviewersApproval?: boolean;
    enableNeedsWorkVeto?: boolean;
};
export type WorkflowProperties = {
    projectKey?: string;
    repoSlug?: string;
    refName?: string;
    refPattern?: string;
    srcRefName?: string;
    srcRefPattern?: string;
    pushAfterPullReq?: boolean;
    unapprovePullReq?: boolean;
    unapprovePullReqTargetRefChange?: boolean;
    enableMergeConditionVeto?: boolean;
    inheritedMergeConditionVetoEnabled?: boolean;
};
export function signApprove(pullReqId: number, projectKey: string, repoSlug: string, signApprove?: SignApprove, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/approvals/${projectKey}/${repoSlug}/${pullReqId}/sign`, oazapfts.json({
        ...opts,
        method: "POST",
        body: signApprove
    })));
}
export function getSignApprovers(pullReqId: number, projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestApplicationUser[];
    }>(`/approvals/${projectKey}/${repoSlug}/${pullReqId}/approvers`, {
        ...opts
    }));
}
export function getSignatures(pullReqId: number, projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestPullRequestSignature[];
    }>(`/approvals/${projectKey}/${repoSlug}/${pullReqId}/signatures`, {
        ...opts
    }));
}
export function isSignRequired(pullReqId: number, projectKey: string, repoSlug: string, userId: number, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/approvals/${projectKey}/${repoSlug}/${pullReqId}/${userId}/signrequired`, {
        ...opts
    }));
}
export function getGlobalConfig(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/globalconfig/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setGlobalConfig1(projectKey: string, repoSlug: string, globalConfig?: GlobalConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/globalconfig/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: globalConfig
    })));
}
export function getGlobalConfig1(opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: GlobalConfig;
    }>("/globalconfig", {
        ...opts
    }));
}
export function setGlobalConfig2(globalConfig?: GlobalConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>("/globalconfig", oazapfts.json({
        ...opts,
        method: "POST",
        body: globalConfig
    })));
}
export function getGlobalConfig2(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/globalconfig/${projectKey}`, {
        ...opts
    }));
}
export function setGlobalConfig(projectKey: string, globalConfig?: GlobalConfig, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/globalconfig/${projectKey}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: globalConfig
    })));
}
export function resetGroupUsersCache(command: string, authkey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/globalconfig/caches/${command}/${authkey}`, {
        ...opts
    }));
}
export function getProjectReviewers(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchReviewers[];
    }>(`/project/${projectKey}/reviewers`, {
        ...opts
    }));
}
export function setProjectReviewers(projectKey: string, restBranchReviewers?: RestBranchReviewers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/reviewers`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchReviewers
    })));
}
export function deleteProjectReviewers(projectKey: string, restBranchReviewers?: RestBranchReviewers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/reviewers`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchReviewers
    })));
}
export function getProjectReviewersList(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchReviewers[];
    }>(`/project/${projectKey}/reviewerslist`, {
        ...opts
    }));
}
export function setProjectReviewersList(projectKey: string, body?: RestBranchReviewers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/reviewerslist`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function getProjectSignapprovers(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchSignapprovers[];
    }>(`/project/${projectKey}/signapprovers`, {
        ...opts
    }));
}
export function setProjectSignApprovers(projectKey: string, restBranchSignapprovers?: RestBranchSignapprovers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/signapprovers`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchSignapprovers
    })));
}
export function deleteProjectSignapprovers(projectKey: string, restBranchSignapprovers?: RestBranchSignapprovers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/signapprovers`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchSignapprovers
    })));
}
export function getProjectSignapproversList(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchSignapprovers[];
    }>(`/project/${projectKey}/signapproverslist`, {
        ...opts
    }));
}
export function setProjectSignApproversList(projectKey: string, body?: RestBranchSignapprovers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/signapproverslist`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function getProjectAutomergers(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchAutoMergers[];
    }>(`/project/${projectKey}/automerge`, {
        ...opts
    }));
}
export function setProjectAutoMergers(projectKey: string, restBranchAutoMergers?: RestBranchAutoMergers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/automerge`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchAutoMergers
    })));
}
export function deleteProjectAutomergers(projectKey: string, restBranchAutoMergers?: RestBranchAutoMergers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/automerge`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchAutoMergers
    })));
}
export function getProjectAutomergersList(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchAutoMergers[];
    }>(`/project/${projectKey}/automergelist`, {
        ...opts
    }));
}
export function setProjectAutoMergersList(projectKey: string, body?: RestBranchAutoMergers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/automergelist`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function deleteProjectAutomergersList(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/project/${projectKey}/automergelist`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getSignapprovers(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchSignapprovers[];
    }>(`/branch/signapprovers/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setSignApprovers(projectKey: string, repoSlug: string, restBranchSignapprovers?: RestBranchSignapprovers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/signapprovers/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchSignapprovers
    })));
}
export function deleteSignapprovers(projectKey: string, repoSlug: string, restBranchSignapprovers?: RestBranchSignapprovers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/signapprovers/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchSignapprovers
    })));
}
export function getBranchReviewers(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchReviewers[];
    }>(`/branch/reviewers/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setBranchReviewers(projectKey: string, repoSlug: string, restBranchReviewers?: RestBranchReviewers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/reviewers/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchReviewers
    })));
}
export function deleteBranchReviewers(projectKey: string, repoSlug: string, restBranchReviewers?: RestBranchReviewers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/reviewers/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchReviewers
    })));
}
export function getBranchReviewersList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchReviewers[];
    }>(`/branch/reviewerslist/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setBranchReviewersList(projectKey: string, repoSlug: string, body?: RestBranchReviewers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/reviewerslist/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function deleteBranchReviewersList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/reviewerslist/${projectKey}/${repoSlug}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getSignapproversList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchSignapprovers[];
    }>(`/branch/signapproverslist/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setSignApproversList(projectKey: string, repoSlug: string, body?: RestBranchSignapprovers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/signapproverslist/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function deleteSignapproversList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/signapproverslist/${projectKey}/${repoSlug}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getBranchAutomergers(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchAutoMergers[];
    }>(`/branch/automerge/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setBranchAutoMergers(projectKey: string, repoSlug: string, restBranchAutoMergers?: RestBranchAutoMergers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/automerge/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: restBranchAutoMergers
    })));
}
export function deleteBranchAutomergers(projectKey: string, repoSlug: string, restBranchAutoMergers?: RestBranchAutoMergers, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/automerge/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "DELETE",
        body: restBranchAutoMergers
    })));
}
export function getBranchAutomergersList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: RestBranchAutoMergers[];
    }>(`/branch/automergelist/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function setBranchAutomergersList(projectKey: string, repoSlug: string, body?: RestBranchAutoMergers[], opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/automergelist/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body
    })));
}
export function deleteBranchAutomergersList(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/branch/automergelist/${projectKey}/${repoSlug}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getWorkflowProperties(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: WorkflowProperties;
    }>(`/workflow/${projectKey}`, {
        ...opts
    }));
}
export function updateWorkflowProperties(projectKey: string, workflowProperties?: WorkflowProperties, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}`, oazapfts.json({
        ...opts,
        method: "PUT",
        body: workflowProperties
    })));
}
export function setWorkflowProperties(projectKey: string, workflowProperties?: WorkflowProperties, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: workflowProperties
    })));
}
export function removeWorkflowProperties(projectKey: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}`, {
        ...opts,
        method: "DELETE"
    }));
}
export function getWorkflowProperties1(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: WorkflowProperties;
    }>(`/workflow/${projectKey}/${repoSlug}`, {
        ...opts
    }));
}
export function updateWorkflowProperties1(projectKey: string, repoSlug: string, workflowProperties?: WorkflowProperties, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "PUT",
        body: workflowProperties
    })));
}
export function setWorkflowProperties1(projectKey: string, repoSlug: string, workflowProperties?: WorkflowProperties, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}/${repoSlug}`, oazapfts.json({
        ...opts,
        method: "POST",
        body: workflowProperties
    })));
}
export function removeWorkflowProperties1(projectKey: string, repoSlug: string, opts?: Oazapfts.RequestOpts) {
    return oazapfts.ok(oazapfts.fetchJson<{
        status: number;
        data: Blob;
    }>(`/workflow/${projectKey}/${repoSlug}`, {
        ...opts,
        method: "DELETE"
    }));
}
